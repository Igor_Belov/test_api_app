# Readme

## Install

```sh
bundle install
rails db:setup
```

## Use

```sh
rails s
```

## API

### Create user

Request `POST /users` with json: 

```json
    {
        "email": "user@example.com",
        "password": "password123"
    }
```

### Login

Request `POST /login` with json:

```json
    {
        "email": "user@example.com",
        "password": "password123"
    }
```

Response:

```json
    {
        "token": "XXX"
    }
```

Use token to access data:

```
    Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ.JC6qKuH9SG0SIiYSfhZUFTtirxN9Q47buLk0DPFFFzE
```

### Get all articles

Request `GET /articles`

### Get all categories

Request `GET /categories`

### Get all authors

Request `GET /authors`

### Create category article

Request `POST /categories/:category_id/articles` with json:

```json
    {
        "title": "My articles",
        "body": "Lorem Ipsum"
    }
```

### Get category articles

Request `GET /categories/:category_id/articles`

### Get author articles

Request `GET /authors/:author_id/articles`

### Get article

Request `GET /articles/:id`

### Get article comments

Request `GET /articles/:id/comments`

### Add article comment

Request `POST /articles/:id/comments` with json:

```json
    {
        "body": "Hello world"
    }
```

### Delete comment

Request `DELETE /comments/:id`

### Delete article

Request `DELETE /articles/:id`
