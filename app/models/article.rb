class Article < ApplicationRecord
  belongs_to :author, class_name: "User"
  belongs_to :category

  has_many :comments, dependent: :destroy

  validates :title, presence: true, length: { maximum: 100 }
  validates :body, presence: true


  def count_comments
    self.comments.count
  end
end 
