class User < ApplicationRecord
	has_many :articles, foreign_key: "author_id", dependent: :destroy
	has_many :comments, foreign_key: "author_id", dependent: :destroy

	has_secure_password

	validates :password, length: { minimum: 6 }
	validates :email, uniqueness: true, email: true
	validates :full_name, presence: true
end
