class ArticlePreviewSerializer < ActiveModel::Serializer
  attributes :id, :title, :count_comments, :created_at

  belongs_to :author, serializer: AuthorSerializer
  belongs_to :category

  attribute :body do
    if self.object.body.length > 500
      "#{self.object.body.slice(0, 500)}…"
    else
      self.object.body
    end
  end
end
