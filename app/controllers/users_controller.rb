class UsersController < ApplicationController
	skip_before_action :authorized, only: [:create, :login]

 	def create
	  	user = User.create(user_params)

	  	if user.valid?
	  		token = encode_token({user_id: user.id})
	  		render json: {token: token}
	  	else
	  		render json: {error: user.errors.full_messages}, status: :bad_request
	  	end
  	end


	def login
		user = User.find_by(email: params[:email])

		if user && user.authenticate(params[:password])
			token = encode_token({user_id: user.id})
			render json: {token: token}
		else
			render json: {error: "Invalid email or password"}, status: :bad_request
		end
	end

	private

	def user_params
		params.permit(:email, :password, :full_name)
	end
end