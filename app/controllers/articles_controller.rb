class ArticlesController < ApplicationController

	def create
		article = logged_in_user.articles.create(article_params)
		
		if article.valid?
			return render json: article
		end

		render json: { errors: article.errors.full_messages }, status: :bad_request
	end

	def show
		article = Article.find_by(id: params[:id])

		if article.nil?
			return render json: {error: "Article not found"}, status: :not_found
		end

		render json: article
	end

	def index
		if params[:category_id]
			category = Category.find_by(id: params[:category_id])

			if category.nil?
				return render json: { error: "Category not found" }, status: :not_found
			end

			return render json: category.articles.order(created_at: :desc), each_serializer: ArticlePreviewSerializer
		elsif params[:author_id]
			author = User.find_by(id: params[:author_id])

			if author.nil?
				return render json: { error: "Author not found" }, status: :not_found
			end

			return render json: author.articles.order(created_at: :desc), each_serializer: ArticlePreviewSerializer
		end

		
		render json: Article.order(created_at: :desc), each_serializer: ArticlePreviewSerializer
	end

	def destroy
		article = logged_in_user.articles.find_by(id: params[:id])

		if article.nil?
			return render json: {error: "Article not found"}, status: :not_found
		end

		article.destroy

		render status: :no_content
	end

	private

	def article_params
		params.permit(:title, :body, :category_id)
	end
end
