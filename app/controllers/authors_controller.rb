class AuthorsController < ApplicationController
	def index
		render json: User.order(full_name: :asc), each_serializer: AuthorSerializer
	end
end