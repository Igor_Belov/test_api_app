class CommentsController < ApplicationController
	def create
		comment = logged_in_user.comments.create(comment_params)

		if comment.valid?
			return render json: comment
		end

		render json: { errors: comment.errors.full_messages }
	end

	def index
		article = Article.find_by(id: params[:article_id])

		if article.nil?
			return render json: { error: "Article not found" }, status: :not_found
		end

		render json: article.comments.order(created_at: :desc)
	end

	def destroy
		comment = logged_in_user.comments.find_by(id: params[:id])

		if comment.nil?
			return render json: { error: "Comment not found" }, status: :not_found
		end

		comment.destroy()

		render status: :no_content
	end

	private

	def comment_params
		params.permit(:body, :article_id)
	end
end
