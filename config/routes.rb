Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :users, only: [:create]
  
  post "/login", to: "users#login"

  resources :categories, only: [:index] do
    resources :articles, only: [:index, :create] 
  end

  resources :articles, only: [:index, :show, :destroy] do
    resources :comments, only: [:create, :index]
  end

  resources :comments, only: [:destroy]

  resources :authors, only: [:index] do
    resources :articles, only: [:index]
    
  end
end
