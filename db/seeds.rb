# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
cat1 = Category.create!(name: "Category 1")
cat2 = Category.create!(name: "Category 2")
cat3 = Category.create!(name: "Category 3")

user1 = User.create!(email: "user1@example.com", password: "password123", full_name: "User1")
user2 = User.create!(email: "user2@example.com", password: "password123", full_name: "User2")
user3 = User.create!(email: "user3@example.com", password: "password123", full_name: "User3")

article1 = user1.articles.create!(category: cat1, title: "Article 1", body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ultricies enim in magna vehicula molestie. Maecenas venenatis ultricies diam vitae facilisis. Ut scelerisque elit sit amet sem suscipit, nec efficitur urna accumsan. Phasellus et efficitur odio. Nam rutrum lectus id ex fringilla, a venenatis purus maximus.")
article2 = user1.articles.create!(category: cat1, title: "Article 2", body: "Mauris odio enim, aliquet mollis nisl ut, maximus iaculis quam. Suspendisse eget enim vitae leo facilisis rhoncus vitae sed sem. Phasellus sit amet nibh nisl. Vivamus a ante tincidunt, finibus enim eu, gravida justo. Aliquam accumsan mollis dui, at eleifend enim finibus id. Vestibulum blandit pharetra ligula, a eleifend orci cursus a.")
article3 = user1.articles.create!(category: cat2, title: "Article 3", body: "Fusce eget neque at augue accumsan ullamcorper. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed justo magna. Nulla facilisi. Duis rutrum metus at ligula aliquet, nec sodales arcu imperdiet. Nunc eget dignissim nisi. Proin bibendum sapien id porttitor rutrum. Sed et nunc sed risus efficitur blandit eu ut libero. Suspendisse quis mi elit. In sed ipsum eu mauris pharetra pellentesque id at enim. Etiam eu pretium leo. Curabitur convallis ultrices magna sit amet mattis. Suspendisse posuere urna a libero porttitor gravida. Vivamus feugiat risus in ante commodo luctus. Sed sit amet pellentesque tortor, sit amet sollicitudin ex. Donec vulputate, eros ac sagittis fermentum, magna sapien finibus justo, eu finibus lacus dui id nibh.")
article4 = user2.articles.create!(category: cat2, title: "Article 4", body: "In commodo diam nec egestas bibendum. Vivamus consectetur dignissim eleifend. Nam eu sem bibendum, fermentum tellus non, placerat erat. Pellentesque sed sapien volutpat, pulvinar arcu sed, vehicula turpis. Nam a turpis nec augue hendrerit mattis ac vitae arcu. Vivamus et lectus ut libero viverra dignissim.")

article1.comments.create!(author: user3, body: "Nulla eleifend dictum consectetur.")
article1.comments.create!(author: user1, body: "Praesent eleifend, dui a ultricies rutrum, ante leo fringilla felis, nec fermentum quam tortor nec magna. Duis tempor eget leo placerat elementum.")

article4.comments.create!(author: user1, body: "Mauris eget ultrices ipsum.")
article4.comments.create!(author: user3, body: "Donec molestie dolor sit amet turpis laoreet, id placerat magna bibendum.")
article4.comments.create!(author: user3, body: "Quisque blandit libero elit, sit amet laoreet orci vulputate nec. Maecenas bibendum nec risus ut scelerisque.")
